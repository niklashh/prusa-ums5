#!/bin/sh -x

echo "Creating new.ufp from $1"

hull_min=`grep '^;\sfirst_layer_print_min' "$1" |cut -d ' ' -f 3`
hull_max=`grep '^;\sfirst_layer_print_max' "$1" |cut -d ' ' -f 3`

header=";START_OF_HEADER
;HEADER_VERSION:0.1
;FLAVOR:Griffin
;GENERATOR.NAME:Cura_SteamEngine
;GENERATOR.VERSION:main
;GENERATOR.BUILD_DATE:2022-09-13
;TARGET_MACHINE.NAME:Ultimaker S5
;EXTRUDER_TRAIN.0.INITIAL_TEMPERATURE:205
;EXTRUDER_TRAIN.0.MATERIAL.VOLUME_USED:6030
;EXTRUDER_TRAIN.0.MATERIAL.GUID:506c9f0d-e3aa-4bd4-b2d2-23e2425b1aa9
;EXTRUDER_TRAIN.0.NOZZLE.DIAMETER:0.4
;EXTRUDER_TRAIN.0.NOZZLE.NAME:AA 0.4
;BUILD_PLATE.TYPE:glass
;BUILD_PLATE.INITIAL_TEMPERATURE:60
;BUILD_VOLUME.TEMPERATURE:28
;PRINT.TIME:1918
;PRINT.GROUPS:1
;PRINT.SIZE.MIN.X:%s
;PRINT.SIZE.MIN.Y:%s
;PRINT.SIZE.MIN.Z:0
;PRINT.SIZE.MAX.X:%s
;PRINT.SIZE.MAX.Y:%s
;PRINT.SIZE.MAX.Z:290
;END_OF_HEADER"

cp base.ufp new.ufp

printf "$header" "$hull_min" "$hull_min" "$hull_max" "$hull_max" > concatenated.gcode

cat "$1" >> concatenated.gcode
# zip --delete new.ufp '**/model.gcode'
zip --update new.ufp concatenated.gcode
printf '@ concatenated.gcode\n@=/3D/model.gcode' |zipnote -w new.ufp
